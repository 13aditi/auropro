use nyse;

create table Fundamentals(
Ticker_Symbol varchar(5),
Period_Ending date ,
Total_Liablities_And_Equity float(15) not null,
Total_Revenue float(15) not null,
Earnings_Per_Share decimal(4,2) not null,
primary key(Ticker_Symbol,Period_Ending));

create table gics(
gics_sub_industry varchar(50) primary key,
gics_sector varchar(40) not null
);

create table Securities(
security varchar(40) primary key,
Ticker_Symbol varchar(5) not null,
gics_sub_industry varchar(50) not null, 
foreign key(ticker_symbol) references fundamentals(ticker_symbol),
foreign key(gics_sub_industry) references gics(gics_sub_industry)
);

create table lookup(
gics_sector varchar(40) primary key);